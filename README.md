# sigdim

A library of combinatorial and linear programming algorithms for the characterization of the extremal measurements and the computation of the signaling dimension of any given system in generalized probabilistic theories.

If you use this program for a publication, please consider citing the following references:
* M. Dall'Arno, S. Brandsen, A. Tosini, F. Buscemi, and V. Vedral, No-hypersignaling principle, Phys. Rev. Lett. 119, 020401 (2017).
* M. Dall'Arno, The signaling dimension of physical systems, Quantum Views 6, 66 (2022).
* M. Dall'Arno, A. Tosini, and F. Buscemi, The signaling dimension in generalized probabilistic theories, arXiv:2311.13103.
