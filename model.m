## Copyright 2016, 2023 Michele Dalll'Arno

## This program is free software: you can redistribute it
## and/or modify it under the terms of the GNU General
## Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your
## option) any later version.

## This program is distributed in the hope that it will be
## useful, but WITHOUT ANY WARRANTY; without even the
## implied warranty of MERCHANTABILITY or FITNESS FOR A
## PARTICULAR PURPOSE. See the GNU General Public License
## for more details.

## You should have received a copy of the GNU General Public
## License along with this program. If not, see
## <https://www.gnu.org/licenses/>.

##usage: m = model(W, E)
##
##Finds all the pairs (W, E), where W is an entangled
##bipartite state and E is an entangled bipartite effect,
##such that a certain circuit gives non-nengative
##probabilities.
function m = model(W, E)
  S = swap();
  m = [];
  n = cartprod([0, 1], [0, 1], [0, 1]);
  for x = [17 : 24]
    for y = [17 : 24]
      U = kron(E(:, y), eye(3))' * kron(eye(3), W(:, x));
      V = kron(E(:, y), eye(3))' * kron(eye(3), S * W(:, x))
      ;
      for k = [1 : size(n, 1)]
	p = kron(E(:, y), E(:, y))' * ...
	    kron(eye(3), S, eye(3)) * kron(S, eye(9)) * ...
	    kron(U^(n(k, 1)), eye(3), U^(n(k, 2)),
		 U^(n(k, 3))) * ...
	    kron(W(:, x), W(:, x));
	if (p < 0) break endif
	p = kron(E(:, y), E(:, y))' * ...
	    kron(eye(3), S, eye(3)) * kron(S, eye(9)) * ...
	    kron(V^(n(k, 1)), eye(3), V^(n(k, 2)),
		 V^(n(k, 3))) * ...
	    kron(S * W(:, x), S * W(:, x));
	if (p < 0) break endif
      endfor
      if (p >= 0) m = [m, [x; y]]; endif
    endfor
  endfor
endfunction

function S = swap()
  S = [1 0 0 0 0 0 0 0 0;
       0 0 0 1 0 0 0 0 0;
       0 0 0 0 0 0 1 0 0;
       0 1 0 0 0 0 0 0 0;
       0 0 0 0 1 0 0 0 0;
       0 0 0 0 0 0 0 1 0;
       0 0 1 0 0 0 0 0 0;
       0 0 0 0 0 1 0 0 0;
       0 0 0 0 0 0 0 0 1];
endfunction
