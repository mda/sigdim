## Copyright 2023 Michele Dalll'Arno

## This program is free software: you can redistribute it
## and/or modify it under the terms of the GNU General
## Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your
## option) any later version.

## This program is distributed in the hope that it will be
## useful, but WITHOUT ANY WARRANTY; without even the
## implied warranty of MERCHANTABILITY or FITNESS FOR A
## PARTICULAR PURPOSE. See the GNU General Public License
## for more details.

## You should have received a copy of the GNU General Public
## License along with this program. If not, see
## <https://www.gnu.org/licenses/>.

##usage: decompose(W, E, M)
##
##Computes the signaling dimension by decomposing all
##extremal conditional probability distributions in terms of
##classical distributions or, failing that, finding a
##violated linear witness.
function decompose(W, E, M)
  param.msglev = 0;
  for k = [1 : size(M, 1)]
    p = distribution(k, W, E, M);
    [m, n] = size(p);
    q = unique(p, "rows", "stable");
    b = vec(q);

    d = 4;
    do
      tic;
      B = vertices(q, d);
      A = fullmat(B, size(q, 2));
      c = zeros(size(A, 2), 1);
      [x, f, err] = glpk(c, A, b, [], [], [], [], [], param);
      ub = ones(size(A, 1), 1);
      lb = -ub;
      ctype = repmat("U", size(A, 2), 1);
      vartype = repmat("C", size(A, 1), 1);
      [y, g, err2] = glpk(b, A', c, lb, ub, ctype, vartype, -1, param);
      printf("%8d  %8d  %8d  %8d  %8d  %+8f  %8f\n", k, d, size(A, 2), classical(m, n, d), err, g, toc);
      save(sprintf("lp-M%02dd%d.dat", k, d), "A", "b", "x", "y", "g");
      d = d + 1;
    until (err == 0)
  endfor
endfunction

function B = fullmat(A, n)
  B = zeros(n * size(A, 1), size(A, 2));
  a = zeros(size(A, 1), n);
  for l = [1 : size(A, 2)]
    for j = [1 : size(A, 1)]
      a(j, :) = eye(n)(A(j, l), :);
    endfor
    B(:, l) = vec(a);
  endfor
endfunction

function p = distribution(n, W, E, M)
  p = W(:, 1 : 16)' * (E * diag(M(n, :)));
  p = p(:, ~all(p == 0, 1));
  p = p / sum(p(1, :));
endfunction

function A = vertices(p, d)
  [m, n] = size(p);
  A = zeros(m, prod(sum(p != 0, 2)), "uint8");
  l = 0;
  a = zeros(m, n);
  function recfun(k = 1)
    for j = find(p(k, :))
      a(k, :) = eye(n)(j, :);
      if (sum(any(a(1:k,:))) <= d)
	if (k == m)
	  A(:, ++l) = find(a') - [0 : n : (m-1)*n]';
	else
	  recfun(k + 1);
	endif
      endif
    endfor
  endfunction
  recfun();
  A = A(:, [1 : l]);
endfunction

function V = classical(m, n, d)
  k = [1 : d];
  V = sum(factorial(k) .* bincoeff(n, k) .* striling2(m, k));
endfunction

function S2 = striling2(m, K)
  S2 = 0;
  for k = K
    j = [1 : k];
    S2 += 1/factorial(k) * sum((-1).^(k-j) .* bincoeff(k, j) .* j.^m);
  endfor
endfunction
