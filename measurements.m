## Copyright 2023 Michele Dalll'Arno

## This program is free software: you can redistribute it
## and/or modify it under the terms of the GNU General
## Public License as published by the Free Software
## Foundation, either version 3 of the License, or (at your
## option) any later version.

## This program is distributed in the hope that it will be
## useful, but WITHOUT ANY WARRANTY; without even the
## implied warranty of MERCHANTABILITY or FITNESS FOR A
## PARTICULAR PURPOSE. See the GNU General Public License
## for more details.

## You should have received a copy of the GNU General Public
## License along with this program. If not, see
## <https://www.gnu.org/licenses/>.

##usage: N = measurements(E, M)
##
##Finds a rtepresentative for each equivalence class under
##reversible transformations within the set of extremal
##measurements over extremal normalized effects.
function N = measurements(E, V)
  T = zeros(24, 128);
  R = reversible(E);
  M = zeros(size(V));
  row = 0;
  for v = V'
    for i = 1 : 128
      T(:, i) = R(:, :, i) * v;
    endfor
    u = sortrows(T')(1, :);
    M(++row, :) = u;
  endfor
  N = unique(M, "rows");
endfunction

function U = reversible(E)
  U = zeros(24, 24, 128);
  S = swap();
  i = 0;
  for j = 0 : 3
    for k = 0 : 3
      for a = [-1, 1]
	for b = [-1, 1]
	  for c = 0 : 1
	    F = S^c * kron(rotation(j, a), rotation(k, b)) * E;
	    U(:, :, ++i) = permutation(F', E');
	  endfor
	endfor
      endfor
    endfor
  endfor
endfunction

function U = rotation(n, k)
  a = [1, 0, -1, 0]';
  U = [a(mod(n, 4) + 1), -k * a(mod(n - 1, 4) + 1), 0;
       a(mod(n - 1, 4) + 1), k * a(mod(n, 4) + 1), 0;
       0, 0, 1];
endfunction

function P = permutation(A, B)
  [ism, idx] = ismember(A, B, "rows");
  P = eye(size(A, 1))(idx, :);
endfunction

function S = swap()
  S = eye(9)([1,4,7,2,5,8,3,6,9], :);
endfunction
